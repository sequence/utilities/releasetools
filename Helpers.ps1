################################################################################

function Add-VersionIncrement {
    [CmdletBinding()]
    Param (
        # Type of increment - major, minor, patch...
        [Parameter(Mandatory=$true,
                   Position=1)]
        [Alias("Type")]
        [ValidateSet('Major', 'Minor', 'Patch', 'Alpha', 'Beta')]
        [string]$IncrementType,

        # Semantic version
        [Parameter(Mandatory=$true,
                   Position=2,
                   ValueFromPipeline=$true)]
        [string]$Version
    )

    Begin {
        function incrementPrerelease($ver, $type) {
            if ($ver[4]) {
                if ($ver[4] -match "^-$type.(\d+)") {
                    "$($ver[1]).$($ver[2]).$($ver[3])-$type.$([int]$Matches[1] + 1)"
                } elseif ($ver[4] -match "^-$type$") {
                    "$($ver[1]).$($ver[2]).$($ver[3])-$type.2"
                } else {
                    "$($ver[1]).$($ver[2]).$($ver[3])-$type.1"
                }
            } else {
                "$($ver[1]).$($ver[2]).$($ver[3])-$type.1"
            }
        }
    }

    Process {
        if ($Version.TrimStart('v') -match '^(\d+)\.(\d+)\.(\d+)(-.*)?') {
            $ver = $Matches
            $newVersion = switch ($IncrementType) {
                'Major' { "$([int]$ver[1] + 1).0.0" }
                'Minor' { "$($ver[1]).$([int]$ver[2] + 1).0" }
                'Patch' { "$($ver[1]).$($ver[2]).$([int]$ver[3] + 1)" }
                'Alpha' { incrementPrerelease $ver 'alpha' }
                'Beta' { incrementPrerelease $ver 'beta' }
                default {
                    throw "oh no"
                }
            }
        } else {
            throw "Incorrect version for last release"
        }
        Write-Output $newVersion
    }

}

################################################################################
