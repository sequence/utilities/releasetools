<#
.SYNOPSIS
    A script to generate project changelogs using git history and
    GitLab issue labels.
.DESCRIPTION
    This script retrieves all GitLab merge requests between the FromDate
    and ToDate. If neither is specified, all merge requests since last
    release are retrieved.

    The issues are grouped into 'Types' using issue labels. These
    can be overridden using the Types parameter. By default, the types are:

    - New Features (Labels: 'feature')
    - Bug Fixes (Labels: 'bug::possible', 'bug::confirmed', 'bug::upstream', 'bug::regression')
    - Maintenance (Labels: 'backstage', 'technical debt')
    - Documentation (Labels: 'documentation')
    - Other - All issues that do not have one of the labels for the preceding Types

    If an issue has the label 'help wanted', it's considered
    to be a community contribution and the commit Author's name is
    included in the changelog.
.EXAMPLE
    New-Changelog.ps1 -ProjectId 123456789 -ReleaseVersion v0.1.1 -ReleaseDate '2020-01-01'
#>

[CmdletBinding()]
Param (
    # The GitLab project ID.
    [Parameter(Mandatory=$true,
               Position=1)]
    [int]$ProjectId,

    # The version of the current release.
    [Parameter(Mandatory=$true)]
    [string]$ReleaseVersion,

    # Release date. Default is today as yyyy-MM-dd.
    [Parameter(Mandatory=$false)]
    [string]$ReleaseDate = (Get-Date -Format 'yyyy-MM-dd'),

    # The path to the changelog. Default is './CHANGELOG.md'
    [Parameter(Mandatory=$false)]
    [string]$ChangelogPath,

    # Start searching for merge requests from this date.
    # Default is the date of the last release.
    [Parameter(Mandatory=$false)]
    [datetime]$FromDate,

    # Only include merge requests up to this date. No filter by default.
    [Parameter(Mandatory=$false)]
    [datetime]$ToDate,

    # The target branch for the merge requests. Default is 'main'.
    [Parameter(Mandatory=$false)]
    [string]$TargetBranch = 'main',

    # The GitLab personal access token, if creating a changelog for a private project.
    [Parameter(Mandatory=$false)]
    [string]$GitLabApiToken,

    # Array of psobjects for grouping issues.
    # By default issues are assigned to the 'Other' group.
    #
    # [pscustomobject]@{
    #     Name = 'Type of Issue'
    #     Labels = @(
    #         'gitlab labels to match'
    #         'bug::possible'
    #         'bug::confirmed'
    #     )
    #     Issues = @() # Required. Leave blank, or add custom issues here
    # }
    #
    # If adding custom issues, they should have the form:
    # [pscustomobject]@{
    #     Author  = 'Name'
    #     IssueId = 123
    #     Issue   = 'Issue Title'
    #     Labels  = @()
    # }
    [Parameter(Mandatory=$false)]
    [pscustomobject[]]$Types = @(
        [pscustomobject]@{
            Name = 'New Features'
            Labels = @(
                'feature'
            )
            Issues = @()
        }
        [pscustomobject]@{
            Name = 'Bug Fixes'
            Labels = @(
                'bug::possible'
                'bug::confirmed'
                'bug::upstream'
                'bug::regression'
            )
            Issues = @()
        }
        [pscustomobject]@{
            Name = 'Maintenance'
            Labels = @(
                'backstage'
                'technical debt'
            )
            Issues = @()
        }
        [pscustomobject]@{
            Name = 'Documentation'
            Labels = @(
                'documentation'
            )
            Issues = @()
        }
    ),

    # Label to match for community contributions. Author name
    # will be added to the release notes.
    [Parameter(Mandatory=$false)]
    [string]$CommunityLabel = 'help wanted',

    # Issues with these labels will not be added to the changelog
    [Parameter(Mandatory=$false)]
    [string[]]$IgnoreLabel = @(
        'excludecl'
        'wf::abandoned'
    ),

    # Issues with these labels should have a 'Changelog Summary'
    # section which will be used for the changelog summary
    [Parameter(Mandatory=$false)]
    [string]$SummaryLabel = 'keyfeature',

    # Summary categories for grouping issues that have a 'Changelog Summary'
    # This needs to be an ordered dictionary with category name - psobject
    # configuration entries:
    #     'Logging and Monitoring' = [pscustomobject]@{
    #         Category   = 'Logging and Monitoring'
    #         Labels     = @('area::logging', 'area::monitoring')
    #         Issues     = @()
    #         IsFallback = $false
    #         NoHeader   = $false
    #     }
    # 
    # At least one category must have the IsFallback property set to true.
    # This category will be used to group issues that do not fall into any
    # other category.
    [Parameter(Mandatory=$false)]
    [System.Collections.Specialized.OrderedDictionary]$SummaryCategories = [ordered]@{
        'Steps' = [pscustomobject]@{
            Category = 'Steps'
            Labels   = @('area::steps')
            Issues   = @()
        }
        'Sequence Configuration Language' = [pscustomobject]@{
            Category = 'Sequence Configuration Language'
            Labels   = @('area::scl')
            Issues   = @()
        }
        'Core SDK' = [pscustomobject]@{
            Category = 'Core SDK'
            Labels   = @('area::coresdk')
            Issues   = @()
        }
        'Connector Updates' = [pscustomobject]@{
            Category = 'Connector Updates'
            Labels   = @('area::connectors')
            Issues   = @()
        }
        'Data Interchange Format' = [pscustomobject]@{
            Category = 'Data Interchange Format'
            Labels   = @('area::dif')
            Issues   = @()
        }
        'Logging and Monitoring' = [pscustomobject]@{
            Category = 'Logging and Monitoring'
            Labels   = @('area::logging', 'area::monitoring')
            Issues   = @()
        }
        'Other' = [pscustomobject]@{
            Category   = 'Other'
            IsFallback = $true
            NoHeader   = $true
            Issues     = @()
        }
    },

    # Additional changelog description/summary. If set, added to the start
    # of the changelog summary.
    [Parameter(Mandatory=$false)]
    [string]$ChangelogSummary,

    # Description to use when there are merge requests from the last
    # release, but all the issues have IgnoreLabel set.
    [Parameter(Mandatory=$false)]
    [string]$MaintenanceDescription = 'Maintenance release - dependency updates only.',

    # Regex to check that the release version is properly formed
    [Parameter(Mandatory=$false)]
    [string]$SemVerRegex = "^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?$",

    # Output to the pipeline instead of to $ChangelogPath
    [Parameter(Mandatory=$false)]
    [switch]$OutPipeline
)

################################################################################

if (!(Get-Module GitLabApi)) {
    Import-Module (Join-Path $PSScriptRoot GitLabApi) -WarningAction SilentlyContinue
}

################################################################################

$fallBack = $SummaryCategories.Values | Where-Object { $_.IsFallback }

if (!$fallBack) {
    throw "No fallback summary category defined. Please set 'IsFallback' on at least one category."
}

if ($ReleaseVersion -notmatch $SemVerRegex) {
    throw "ReleaseVersion '$ReleaseVersion' does not match SemVerRegex"
}

################################################################################

if (!$OutPipeline) {
    if ($ChangelogPath) {
        $cp = $ChangelogPath
    } else {
        $cp = [System.IO.Path]::GetFullPath('./CHANGELOG.md')
    }
    Write-Verbose "Changelog path: $cp"
    if (Test-Path -Path $cp) {
        $currentText = Get-Content -Path $cp -Encoding utf8 -Raw
        if ($currentText -match "# $($ReleaseVersion)") {
            throw "ReleaseVersion $($ReleaseVersion) already exists in '$cp'"
        }
    }
}

################################################################################

$mrParams = @{
    ProjectId      = $ProjectId
    State          = 'merged'
    TargetBranch   = $TargetBranch
    NoStatusCheck  = $true
    PerPage        = 100
    GitLabApiToken = $GitLabApiToken
}

if ($PSBoundParameters['FromDate']) {
    $mrParams['CreatedAfter'] = $FromDate.ToString("yyyy-MM-ddTHH:mm:ss")
    $lastRelease = [pscustomobject]@{ name = $mrParams['CreatedAfter'] }
} else {

    Write-Verbose "No FromDate set. Searching for previous release."

    $releases = $null
    $releases = Get-ProjectRelease -ProjectId $ProjectId -GitLabApiToken $GitLabApiToken

    if ($releases) {
        $lastRelease = $releases[0]
        Write-Information "Last release: $($lastRelease.name)"
        $mrParams['CreatedAfter'] = $lastRelease.commit.committed_date.ToString("yyyy-MM-ddTHH:mm:ss")
    } else {
        $lastRelease = [pscustomobject]@{ name = 'the beginning of time...' }
        Write-Information "No previous releases found."
    }

}

if ($PSBoundParameters['ToDate']) {
    $mrParams['CreatedBefore'] = $ToDate.ToString("yyyy-MM-ddTHH:mm:ss")
}

$mergeRequests = Get-MergeRequest @mrParams

if (!$mergeRequests) {
    throw "Could not find any Merge Requests since $($lastRelease.name)"
}

Write-Verbose "Found $($mergeRequests.Count) merge requests"

################################################################################

$other = $Types | Where-Object { $_.Name -eq 'Other' }

if (!$other) {
    $other = [pscustomobject]@{
        Name = 'Other'
        Labels = @()
        Issues = @()
    }
    $Types += $other
}

################################################################################

$totalIssues = 0
$summaries = @()
$issueIds = @{}

foreach ($mr in $mergeRequests) {

    Write-Verbose "Processing '$($mr.title)' ($($mr.iid))"

    $mrIgnore = Compare-Object $IgnoreLabel $mr.labels -ExcludeDifferent

    if ($null -ne $mrIgnore) {
        Write-Verbose "'$($mrIgnore.InputObject)' label found. Skipping."
        continue
    }

    $mrIssues = Get-MergeRequestIssue -ProjectId $ProjectId `
                                      -MergeRequestId $mr.iid `
                                      -GitLabApiToken $GitLabApiToken

    foreach ($issue in $mrIssues) {

        Write-Verbose "  Processing '$($issue.title)' ($($issue.iid))"

        if ($issueIds.ContainsKey($issue.iid)) {
            Write-Verbose "  Issue alread in the changelog. Skipping."
            continue
        }

        $isIgnore = Compare-Object $IgnoreLabel $issue.labels -ExcludeDifferent

        if ($null -ne $isIgnore) {
            Write-Verbose "  '$($isIgnore.InputObject)' label found. Skipping."
            continue
        }

        $mro = [pscustomobject]@{
            IssueId = $issue.iid
            Author  = $issue.author.username
            Issue   = $issue.title
            Labels  = $issue.labels
            Date    = ''
        }

        if ($issue.closed_at) {
            $mro.Date = Get-Date $issue.closed_at
        } else {
            Write-Warning "Issue $($issue.iid) does not have a 'closed_at' date. $($issue.closed_at)"
        }

        if ($issue.labels -contains $SummaryLabel) {
            $m = [regex]::Matches(
                $issue.description,
                '#+ Changelog.*?\r?\n(.*)$',
                [System.Text.RegularExpressions.RegexOptions]::Singleline
            )
            if ($m.Success) {
                $summaries += [pscustomobject]@{
                    Date   = $mro.Date
                    Text   = $m.Groups[1].Value.Trim("`r","`n")
                    Labels = $issue.labels
                }
            } else {
                Write-Warning ("Issue $($issue.iid) has the '$SummaryLabel' label but does not" +
                    " have a 'Changelog Summary' section in the issue description.")
            }
        }

        $hasType = $false

        foreach ($type in $Types) {
            foreach ($label in $type.Labels) {
                if ($label -in $mro.Labels) {
                    $type.Issues += $mro
                    Write-Verbose "  Issue type: $($type.Name)"
                    $hasType = $true
                    break
                }
            }
            if ($hasType) {
                break
            }
        }

        if (!$hasType) {
            Write-Verbose "  Issue type: $($other.Name)"
            $other.Issues += $mro
        }

        $issueIds[$issue.iid]++
        $totalIssues++

    }

}

################################################################################

Write-Verbose "Total issues found: $totalIssues"

$md = [System.IO.StringWriter]::new()
$md.NewLine = "`n"

[void]$md.WriteLine("# $ReleaseVersion ($ReleaseDate)")
[void]$md.WriteLine()

if ($ChangelogSummary) {
    [void]$md.WriteLine($ChangelogSummary)
    [void]$md.WriteLine()
}

if ($totalIssues -eq 0) {
    [void]$md.WriteLine($MaintenanceDescription)
    [void]$md.WriteLine()
} else {

    if ($summaries.Count -gt 0) {

        foreach ($summary in ($summaries | Sort-Object -Property Date -Descending)) {
            $hasCat = $false
            foreach ($cat in $SummaryCategories.Values) {
                if ($cat.Labels.Where({ $summary.Labels -contains $_ })) {
                    $cat.Issues += $summary
                    $hasCat = $true
                    break
                }
            }
            if (!$hasCat) {
                $fallBack.Issues += $summary
            }
        }

        [void]$md.WriteLine("## Summary of Changes`n")

        $SummaryCategories.Values | Where-Object { $_.Issues.Count -gt 0 } | ForEach-Object {
            if ($_.NoHeader) {
                [void]$md.WriteLine()
            } else {
                [void]$md.WriteLine("### $($_.Category)`n")
            }
            $_.Issues | ForEach-Object {
                [void]$md.WriteLine($_.Text.Trim("`r","`n"))
            }
            [void]$md.WriteLine()
        }

    }

    ################################################################################

    [void]$md.WriteLine("## Issues Closed in this Release`n")

    foreach ($type in $Types) {
        if ($type.Issues.Count -gt 0) {
            [void]$md.WriteLine("### $($type.Name)$($md.NewLine)")
            $sortedIssues = $type.Issues | Sort-Object -Property Date -Descending
            foreach ($issue in $sortedIssues) {
                if ($issue.MRId) {
                    [void]$md.Write("- $($issue.Issue) !$($issue.MRId)")
                } else {
                    [void]$md.Write("- $($issue.Issue) #$($issue.IssueId)")
                }
                if ($CommunityLabel -in $issue.Labels) {
                    [void]$md.Write(" - @$($issue.Author)")
                }
                [void]$md.WriteLine()
            }
            [void]$md.WriteLine()
        }
    }

}

################################################################################

if ($OutPipeline) {
    $md.ToString() | Write-Output
} else {
    $outText = $md.ToString() + $currentText
    Out-File -InputObject $outText -FilePath $cp -Encoding utf8NoBOM -NoNewline
}

################################################################################
