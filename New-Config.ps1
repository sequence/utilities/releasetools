[CmdletBinding()]
Param (
    [Parameter(Mandatory=$true,
               Position=1)]
    [string]$Destination,

    [Parameter(Mandatory=$true,
               Position=2)]
    [string]$ExtendsJob,

    [Parameter(Mandatory=$false,
               Position=3)]
    [string]$JobPrefix = 'release',

    # Path to the release manifest. If not set, most recent json
    # file in the './releases' directory is used.
    [Parameter(Mandatory=$false,
               Position=4)]
    [string]$ReleaseManifest,

    [Parameter(Mandatory=$false)]
    [switch]$WithNeeds
)

if (!$ReleaseManifest) {
    $ReleaseManifest = Get-ChildItem -Path ./releases |
        Sort-Object -Property Name -Descending |
        Select-Object -First 1 -ExpandProperty FullName
    Write-Information "ReleaseManifest not specified. Using: $ReleaseManifest"
}

$rm = Get-Content $ReleaseManifest -Encoding utf8NoBOM | ConvertFrom-Json -Depth 100

if (!$rm) {
    throw "No release manifest found"
}

$output = @(
"include:
  - local: .release-ci.yml"
)
$rm.Projects | ForEach-Object {
    Write-Information "Creating '$JobPrefix' job for $($_.Name)"
    $output += @"
$JobPrefix $($_.Name):
  extends:
    - '$ExtendsJob'
  variables:
    PROJECT_ID: $($_.Id)
"@
    if ($WithNeeds -and $_.DependsOn) {
        Write-Information "Adding dependencies to needs keyword."
        $inProjects = $_.DependsOn -split '\|' | Where-Object { $_ -in $rm.Projects.Name }
        if ($inProjects) {
          $output += "  needs:"
          $inProjects | ForEach-Object {
              $output += "    - $JobPrefix $_"
          }
        }
    }
}
$output | Out-File $Destination -Encoding utf8NoBOM
