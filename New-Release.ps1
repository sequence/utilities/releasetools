<#
.SYNOPSIS
    
.DESCRIPTION
    
.EXAMPLE
    
#>
[CmdletBinding()]
Param (
    # Name of the release
    [Parameter(Mandatory=$true,
               Position=1)]
    [Alias("Name")]
    [string]$ReleaseName,

    # GitLab PAT
    [Parameter(Mandatory=$true,
               Position=2)]
    [string]$GitLabApiToken,

    # List of projects to create a release for. If not specified, the projects.csv
    # file is imported.
    [Parameter(Mandatory=$false)]
    [psobject[]]$Projects,

    # Do not include these projects in the release
    [Parameter(Mandatory=$false)]
    [string[]]$ProjectFilter,

    # Release date. Default is today as yyyy-MM-dd.
    [Parameter(Mandatory=$false)]
    [string]$ReleaseDate = (Get-Date -Format 'yyyy-MM-dd'),

    # The epic for this release.
    [Parameter(Mandatory=$false)]
    [string]$Epic,

    # The milestone for this release.
    [Parameter(Mandatory=$false)]
    [string]$Milestone,

    # The iteration for this release.
    [Parameter(Mandatory=$false)]
    [string]$Iteration,

    # The destination directory where the release json will be created. Default is .\releases
    [Parameter(Mandatory=$false)]
    [string]$Destination = 'releases',

    # The CSV to import if $Projects is not set. Default is .\projects.csv
    [Parameter(Mandatory=$false)]
    [string]$ProjectsCsv = 'projects.csv',

    # The ID for the organisation group
    [Parameter(Mandatory=$false)]
    [int]$RootGroupId = 8436870, # Sequence

    # The user account for the release.
    [Parameter(Mandatory=$false)]
    [string]$ReleaseUser = 'sq-bot',

    # ReleaseUsers's e-mail address. Used for git commits.
    [Parameter(Mandatory=$false)]
    [string]$ReleaseEmail = 'cicd@sequence.sh',

    # The title for the new issue. Default is 'Prepare for release v$Version'.
    [Parameter(Mandatory=$false)]
    [string]$IssueTitle,

    # Description to use for the release issue.
    [Parameter(Mandatory=$false)]
    [string]$IssueDescription = "Release checklist:

- Project version matches the release version
- Sequence projects have been updated to the latest release version
- Changelog has been updated, including the release summary
- Readme has been updated with the latest changes/examples",

    # Description to use for the release merge request.
    [Parameter(Mandatory=$false)]
    [string]$MergeRequestDescription = "- [ ] Check that project version(s) match the release version
- [ ] Check changelog issues are correctly categorized
- [ ] Add release summary to the changelog if one was not automatically generated
- [ ] Update README with any changes since last release",

    # The label used to exclude merge requests from the changelog.
    # Default is 'excludecl', 'wf::abandoned'.
    [Parameter(Mandatory=$false)]
    [string[]]$IgnoreLabel = @(
        'excludecl'
        'wf::abandoned'
    ),

    # The labels to add to the release issue.
    [Parameter(Mandatory=$false)]
    [string[]]$IssueLabels = @(
        'backstage'
        'area::release/packaging'
        'wf::doing'
    ),

    # The labels to add to the release merge request.
    [Parameter(Mandatory=$false)]
    [string[]]$MergeRequestLabels = @(
        'backstage'
        'area::release/packaging'
    ),

    # Fallback release description, when there are no merge request summaries.
    # Default is 'Dependency updates only'.
    [Parameter(Mandatory=$false)]
    [string]$NoChangelogDescription = 'Maintenance release - dependency updates only.',

    # Filter passed to Update-Dependencies for package updates.
    # Default is 'Sequence'.
    [Parameter(Mandatory=$false)]
    [string]$DependencyFilter = 'Sequence',

    # Regex used to attach job artifacts as packages to the release.
    # Default is 'sign|package dll'.
    [Parameter(Mandatory=$false)]
    [string]$ReleasePackageRegex = 'sign|package dll',

    # Do not filter projects on number of merge request created
    [Parameter(Mandatory=$false)]
    [switch]$IncludeAll
)

################################################################################

if (!(Get-Module GitLabApi)) {
    Import-Module (Join-Path $PSScriptRoot GitLabApi) -WarningAction SilentlyContinue
}

. (Join-Path $PSScriptRoot 'Helpers.ps1')

################################################################################

$emiArgs = @{
    GroupId        = $RootGroupId
    GitLabApiToken = $GitLabApiToken
}

if ($PSBoundParameters['Epic']) {
    $emiArgs['Epic'] = $Epic
}

if ($PSBoundParameters['Milestone']) {
    $emiArgs['Milestone'] = $Milestone
}

if ($PSBoundParameters['Iteration']) {
    $emiArgs['Iteration'] = $Iteration
}

$emi = Get-EpicMilestoneIteration @emiArgs

if (
    $emi.Epic -eq 'Error' -or
    $emi.Milestone -eq 'Error' #-or $emi.Iteration -eq 'Error'
) {
    throw 'Could not retrieve Epic/Milestone/Iteration'
}

if (!$PSBoundParameters['Iteration']) {
    $emi.Iteration = $null
}

################################################################################

$userDetails = Get-User -UserName $ReleaseUser -GitLabApiToken $GitLabApiToken

if (!$userDetails) {
    throw "Could not find user $ReleaseUser"
}

################################################################################

if (!$Projects) {
    $Projects = Import-Csv -Path $ProjectsCsv | ForEach-Object {
        if ([string]::IsNullOrWhiteSpace($_.DependsOn)) {
            $_.DependsOn = @()
        } else {
            $_.DependsOn = $_.DependsOn -split '\|'
        }
        Write-Output $_
    }
}

$filtered = $Projects | Where-Object { $_.Name -notin $ProjectFilter }

################################################################################

$depends = @{}

$filtered | ForEach-Object {
    if ($_.DependsOn) {
        foreach ($d in $_.DependsOn) {
            $depends[$d] += ,$_
        }
    } else {
        $depends['None'] += ,$_
    }
}

$nodes = [System.Collections.ArrayList]::new($depends['None'])
$depends.Remove('None')

$sorted = @()

while ($nodes) {

    $n = $nodes[0]
    $nodes.Remove($n)
    $sorted += $n

    $edges = $depends[$n.Name]
    $depends.Remove($n.Name)

    foreach ($e in $edges) {
        $hasEdge = $false
        foreach ($d in $e.DependsOn) {
            if ($depends.ContainsKey($d)) {
                $hasEdge = $true
                break
            }
        }
        if (!$hasEdge) {
            [void]$nodes.Add($e)
        }
    }

}

################################################################################

$output = @()

foreach ($proj in $sorted) {

    Write-Information "Processing $($proj.Name)"

    $projDetails = Get-Project -ProjectId $proj.Id -GitLabApiToken $GitLabApiToken

    if (!$projDetails) {
        throw "Could not find project id $($proj.Id)"
    }

    $proj.Name = $projDetails.name

    $releases = $null
    $releases = Get-ProjectRelease -ProjectId $proj.Id -GitLabApiToken $GitLabApiToken

    $mrParams = @{
        ProjectId      = $proj.Id
        State          = 'merged'
        TargetBranch   = $projDetails.default_branch
        NoStatusCheck  = $true
        PerPage        = 100
        GitLabApiToken = $GitLabApiToken
    }

    if ($releases) {

        $lastRelease = $releases[0]

        Write-Information "Last release: $($lastRelease.name)"

        if ($proj.VersionIncrement -match '\+') {
            $projVersion = $lastRelease.name.TrimStart('v')
            $proj.VersionIncrement -split '\+' | ForEach-Object {
                $projVersion = Add-VersionIncrement -IncrementType $_ -Version $projVersion
            }
        } else {
            $projVersion = Add-VersionIncrement -IncrementType $proj.VersionIncrement -Version $lastRelease.name
        }

        $lastDate = $lastRelease.commit.committed_date.ToString("yyyy-MM-ddTHH:mm:ss")
        $mrParams['CreatedAfter'] = $lastDate

    } else {
        $lastRelease = [pscustomobject]@{ name = 'n/a' }
        $lastDate = ''
        Write-Information "No previous releases found."
        $projVersion = '0.1.0'
    }

    $mergeRequests = Get-MergeRequest @mrParams

    $changelogCount = ($mergeRequests | Where-Object {
        $null -eq (Compare-Object $IgnoreLabel $_.labels -ExcludeDifferent)
    }).Count

    Write-Information "Merge requests closed since last release: $($mergeRequests.Count)"
    Write-Information "Merge requests without $($IgnoreLabel -join ' ') label(s): $changelogCount"

    if (!$IncludeAll -and $mergeRequests.Count -eq 0) {
        Write-Warning "No updates found since $($lastRelease.name) for $($proj.Name). Skipping"
        continue
    }

    if ($IssueTitle) {
        $projIssue = $IssueTitle -replace '\{version\}', $projVersion
    } else {
        $projIssue = "Prepare for release v$projVersion"
    }

    Add-Member -InputObject $proj -NotePropertyName ReleaseBranch -NotePropertyValue $projDetails.default_branch
    Add-Member -InputObject $proj -NotePropertyName LastReleaseDate -NotePropertyValue $lastDate
    Add-Member -InputObject $proj -NotePropertyName CurrentVersion -NotePropertyValue $lastRelease.name.TrimStart("v")
    Add-Member -InputObject $proj -NotePropertyName ReleaseVersion -NotePropertyValue $projVersion
    Add-Member -InputObject $proj -NotePropertyName IssueTitle -NotePropertyValue $projIssue
    Add-Member -InputObject $proj -NotePropertyName MergeRequests -NotePropertyValue $mergeRequests.Count
    Add-Member -InputObject $proj -NotePropertyName ChangelogCount -NotePropertyValue $changelogCount
    Add-Member -InputObject $proj -NotePropertyName WebUrl -NotePropertyValue $projDetails.web_url
    Add-Member -InputObject $proj -NotePropertyName RepositoryUrl -NotePropertyValue $projDetails.http_url_to_repo

    Write-Information "Updating from $($lastRelease.name) to v$projVersion"

    $output += $proj

}

################################################################################

if (!(Test-Path $Destination)) {
    New-Item -ItemType Directory -Path $Destination | Out-Null
}

$outPrefix = Join-Path $Destination $ReleaseDate
$releaseNo = 1
while ($true) {
    $releaseFile = "$outPrefix.$releaseNo.json"
    if (Test-Path $releaseFile) {
        $releaseNo++
    } else {
        break
    }
}

$json = [pscustomobject]@{
    Name      = $ReleaseName
    Date      = $ReleaseDate
    User      = [pscustomobject]@{
        Id    = $userDetails.id
        Name  = $ReleaseUser
        Email = $ReleaseEmail
    }
    Epic      = $null
    Milestone = $null
    Iteration = $null
    Projects  = $output
    IssueMR   = [pscustomobject]@{
        IgnoreLabel             = $IgnoreLabel
        IssueDescription        = $IssueDescription -replace "`r"
        IssueLabels             = $IssueLabels + @('excludecl')
        MergeRequestDescription = $MergeRequestDescription -replace "`r"
        MergeRequestLabels      = $MergeRequestLabels + @('excludecl')
    }
    NoChangelogDescription = $NoChangelogDescription -replace "`r"
    DependencyFilter       = $DependencyFilter
    ReleasePackageRegex    = $ReleasePackageRegex
}

if ($Epic) {
    $json.Epic = [pscustomobject]@{
        Id   = $emi.Epic.id
        Name = $emi.Epic.title
        Url  = $emi.Epic.web_url
    }
}

if ($Milestone) {
    $json.Milestone = [pscustomobject]@{
        Id   = $emi.Milestone.id
        Name = $emi.Milestone.title
        Url  = $emi.Milestone.web_url
    }
}

if ($Iteration) {
    $json.Iteration = [pscustomobject]@{
        Id   = $emi.Iteration.id
        Name = $emi.Iteration.title
        Url  = $emi.Iteration.web_url
    }
}

ConvertTo-Json -InputObject $json -Depth 100 | Out-File -FilePath $releaseFile -Encoding utf8NoBOM

################################################################################
