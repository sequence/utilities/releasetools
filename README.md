# Sequence Release Tools

A module to automate Sequence releases.

Releases run in three parts:

1. `Start-ProjectRelease.ps1` script runs to:

- Create a new release Issue and Merge Request called `Prepare for release v$ReleaseVersion`
- Run a dependency update for Sequence packages
- Check if the readme has been updated, add a warning to the MR if not
- Generate a new changelog using `New-Changelog.ps1` script
- Set an approval rule on the Merge Request

2. Someone reviews and approves the merge request. Checklist:

- Check that project version(s) match the release version
- Check changelog has been correctly generated and all the issues are correctly categorized
- Add release summary to the changelog if one was not automatically generated
- Update README with any changes since last release

3. `Resume-ProjectRelease.ps1` runs to:

- Run a dependency update for Sequence packages, no prerelease version allowed
- Merges the Merge Request
- Create a new project tag and release
- Attaches any pipeline artifacts to the release as packages

The scripts are designed to resume at the last point of failure, and they
won't re-create a release or re-run any actions, so they can be run multiple
times with the same result. The only exception is Start-ProjectRelease -
if the release Merge Request is open, then each time the script runs it
will re-generate the changelog.

## Creating a Release

1. Use `New-Release` to create a new release manifest.
   It will be placed in the `./releases` folder and time
   stamped with today's date.

```powershell
$token = '<TOKEN>'
.\New-Release.ps1 -ReleaseName "Sequence v0.12.0" -GitLabApiToken $token -InformationAction Continue
```

2. Check that the manifest is correct - especially the versions.

3. Manually run a new pipeline with `RELEASE_JOB` variable set to 'start'.

![image](/uploads/b841ddc9414f71a90e6949d86c5f3188/image.png)

4. Review the merge request and make any required changes. Approve it.

![image](/uploads/4d00f47f8810836ea23cc7e254d5d6e4/image.png)

5. Manually run a new pipeline with `RELEASE_JOB` variable set to 'resume'.

![image](/uploads/9e3f1e6995cc69e5decb573e34de01b3/image.png)

## Additional Manifest Properties

These properties can be set for each project in the release manifest:

| Property             | Description                                                                                         |
| :------------------- | :-------------------------------------------------------------------------------------------------- |
| ChangelogSummary     | Additional text that will be added to the start of the changelog summary (and release description). |
| SkipDependencyUpdate | Do not run a dependency update during `Start-ProjectRelease`.                                       |
