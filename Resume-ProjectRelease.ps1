[CmdletBinding()]
Param (
    # Project ID
    [Parameter(Mandatory=$true,
               Position=1,
               ValueFromPipeline=$true)]
    [int]$ProjectId,

    # GitLab personal access token (PAT)
    [Parameter(Mandatory=$true,
               Position=2)]
    [ValidateNotNullOrEmpty()]
    [string]$GitLabApiToken,

    # Path to the release manifest. If not set, most recent json
    # file in the './releases' directory is used.
    [Parameter(Mandatory=$false,
               Position=3)]
    [string]$ReleaseManifest,

    # If a pipeline is in this state, wait for it
    [Parameter(Mandatory=$false)]
    [string[]]$WaitForPipelineStatus = @('created', 'running', 'pending'),

    # Path to the dotnet-outdated tool for dependency updates
    [Parameter(Mandatory=$false)]
    [string]$DotnetOutdated
)

Begin {

    if (!(Get-Module GitLabApi)) {
        Import-Module (Join-Path $PSScriptRoot GitLabApi) -WarningAction SilentlyContinue
    }

    if (!$ReleaseManifest) {
        $ReleaseManifest = Get-ChildItem -Path ./releases |
            Sort-Object -Property Name -Descending |
            Select-Object -First 1 -ExpandProperty FullName
        Write-Information "ReleaseManifest not specified. Using: $ReleaseManifest"
    }
    
    $rm = Get-Content $ReleaseManifest -Encoding utf8NoBOM | ConvertFrom-Json -Depth 100

}

Process {

    $project = $rm.Projects | Where-Object { $_.Id -eq $ProjectId }

    if (!$project) {
        throw "Could not find project id $ProjectId in the release manifest $ReleaseManifest"
    }

    ################################################################################

    Write-Information "Processing $($project.Name) ($($project.Id))"
    Write-Information $project.WebUrl

    $releaseIssueMR = $null
    $releaseIssueMR = Get-IssueMR -ProjectId $project.Id `
                                  -IssueTitle $project.IssueTitle `
                                  -GitLabApiToken $GitLabApiToken

    if ($releaseIssueMR.Count -gt 1) {
        Write-Information "Found multiple merge requests for '$($project.IssueTitle)'. Using last one."
        $releaseIssueMR = $releaseIssueMR[0]
    }

    if ($releaseIssueMR) {
        Write-Information ("Issue         : $($releaseIssueMR.Issue.web_url)`n" +
                           "Issue state   : $($releaseIssueMR.Issue.state)`n" +
                           "Merge Request : $($releaseIssueMR.MergeRequest.web_url)`n" +
                           "MR state      : $($releaseIssueMR.MergeRequest.state)")
    } else {
        throw "Could not find the release issue $($project.IssueTitle)"
    }

    # re-load using ID to get pipeline information
    $releaseMR = Get-MergeRequest -ProjectId $project.Id `
                                  -MergeRequestId $releaseIssueMR.MergeRequest.iid `
                                  -GitLabApiToken $GitLabApiToken

    if ($releaseMR.state -eq 'opened') {

        if ($releaseMR.merge_status -ne 'can_be_merged') {
            throw "The merge request status needs to be 'can_be_merged' to continue"
        }

        if ($releaseMR.pipeline.status -in $WaitForPipelineStatus) {
            Write-Information "Waiting for merge request pipeline to finish: $($releaseMR.pipeline.web_url)"
            $mrPipeline = Wait-Pipeline -ProjectId $project.Id `
                                        -Ref $releaseMR.pipeline.ref `
                                        -Sha $releaseMR.pipeline.sha `
                                        -GitLabApiToken $GitLabApiToken `
                                        -WaitForPipelineStatus $WaitForPipelineStatus
        } else {
            $mrPipeline = $releaseMR.pipeline
        }

        Write-Information "Pipeline status : $($mrPipeline.status)"

        if ($mrPipeline.status -ne 'success') {
            throw "Merge request pipeline failed."
        }

        try {
            $approvals = Get-MergeRequestApprovalState -ProjectId $project.Id `
                                                       -MergeRequestId $releaseMR.iid `
                                                       -GitLabApiToken $GitLabApiToken `
                                                       -ErrorAction Stop
        } catch {
            Write-Warning "Could not retrieve merge request approval status"
        }

        $approved = $approvals.rules.Count -eq 0 -or $approvals.rules.approved -eq 'True'

        if ($approved) {
            Write-Information "Merge request is approved"
        } else {
            $approvalsReqd = ($approvals.rules.approvals_required | Measure-Object -Sum).Sum
            throw "Merge request still requires $approvalsReqd approval(s)."
        }

        ################################################################################
        # Update approval rule to not require re-approval after dependency updates

        if ($approvals.rules.Count -gt 0) {
            try {
                Remove-MergeRequestApprovalRule -ProjectId $project.Id `
                                                -MergeRequestId $releaseMR.iid `
                                                -ApprovalRuleId $approvals.rules.id `
                                                -GitLabApiToken $GitLabApiToken `
                                                -ErrorAction Stop | Out-Null
            } catch {
                Write-Warning "Could not remove approval rule this might affect auto-merge."
            }
        }

        ################################################################################
        # Only update projects that have dependencies

        if ($project.DependsOn.Length -gt 0) {
            ./dependencies/Update-Dependencies.ps1 -ProjectId $project.Id `
                                                   -IssueTitle $project.IssueTitle `
                                                   -IncludeFilter $rm.DependencyFilter `
                                                   -GitLabApiToken $GitLabApiToken `
                                                   -DotnetOutdated $DotnetOutdated `
                                                   -DoNotUpdateDescription `
                                                   -NoAutoMerge -Wait `
                                                   -ErrorAction Stop
        }

        ################################################################################

        try {
            $accept = Set-MergeRequest -ProjectId $project.Id `
                                       -MergeRequestId $releaseMR.iid `
                                       -GitLabApiToken $GitLabApiToken `
                                       -Merge `
                                       -ErrorAction Stop
        } catch {
            throw ("Could not merge '$($releaseMR.title)'. " +
                "Status Code: $($_.Exception.Response.StatusCode) ($($_.Exception.Response.StatusCode.value__))")
        }

        Write-Information "Merge request is $($accept.state). Merge commit sha: $($accept.merge_commit_sha)"

    } elseif ($releaseMR.state -eq 'merged') {
        # TODO - check pipeline status?
        $accept = $releaseMR
    } else {
        throw "Merge request state '$($releaseMR.state)' is invalid"
    }

    ################################################################################

    $releaseName = 'v' + $project.ReleaseVersion

    $existingRelease = $null

    try {
        $existingRelease = Get-ProjectRelease -ProjectId $project.Id `
                                              -TagName $releaseName `
                                              -GitLabApiToken $GitLabApiToken `
                                              -ErrorAction Stop
    } catch {}

    if ($existingRelease) {
        Write-Information "Release $releaseName already exists"
    } else {

        Write-Information "Waiting for the $($project.ReleaseBranch) branch pipeline to finish"

        $mainPipeline = Wait-Pipeline -ProjectId $project.Id `
                                      -Ref $project.ReleaseBranch `
                                      -Sha $accept.merge_commit_sha `
                                      -GitLabApiToken $GitLabApiToken `
                                      -WaitForPipelineStatus $WaitForPipelineStatus

        Write-Information "Pipeline status : $($mainPipeline.status)"

        if ($mainPipeline.status -ne 'success') {
            throw "Default branch pipeline failed after merge: $($mainPipeline.web_url)"
        }

        ################################################################################
        # Create tag + release

        $changelog = Get-ProjectFile -ProjectId $project.Id `
                                     -Path 'CHANGELOG.md' `
                                     -Ref $project.ReleaseBranch `
                                     -GitLabApiToken $GitLabApiToken `
                                     -Raw
        
        $descMatch = [regex]::Match(
            $changelog,
            "#+ $([regex]::Escape($releaseName)).+?(?:## Summary of Changes)?\r?\n\r?\n(.*?)#+ (Issues Closed in this Release|v\d)",
            [System.Text.RegularExpressions.RegexOptions]::Singleline
        )

        if ($descMatch.Success) {
            $clDescription = $descMatch.Groups[1].Value
        } else {
            throw "Could not retrieve release description from the changelog"
        }

        $tagDescription = ''

        if ($clDescription.Length -le 900) {
            $tagDescription += $clDescription
        }

        $tagDescription += "For more details see the changelog: $($project.WebUrl)/-/blob/$releaseName/CHANGELOG.md"

        try {
            $tagRsp = Add-ProjectTag -ProjectId $project.Id `
                                     -TagName $releaseName `
                                     -Ref $project.ReleaseBranch `
                                     -Message $tagDescription `
                                     -GitLabApiToken $GitLabApiToken `
                                     -ErrorAction Stop
        } catch {
            throw "Could not create tag '$releaseName'"
        }

        Write-Information "Created tag: $($tagRsp.name)"

        $releaseDescription = $clDescription +
            "For details of all the changes, please see the [changelog]($($project.WebUrl)/-/blob/$releaseName/CHANGELOG.md)."

        try {
            $existingRelease = Add-ProjectRelease -ProjectId $project.Id `
                                                  -TagName $tagRsp.name `
                                                  -Description $releaseDescription `
                                                  -GitLabApiToken $GitLabApiToken `
                                                  -ErrorAction Stop
        } catch {
            throw "Could not create release '$releaseName'"
        }

        Write-Information "Created release: $($existingRelease.name)"

        Start-Sleep -Seconds 10

    }

    ################################################################################
    # Wait for the release pipeline to complete

    Write-Information "Waiting for the release pipeline to finish"

    $releasePipe = Wait-Pipeline -ProjectId $project.Id `
                                 -Ref $releaseName `
                                 -GitLabApiToken $GitLabApiToken `
                                 -WaitForPipelineStatus $WaitForPipelineStatus

    Write-Information "Pipeline status : $($releasePipe.status)"

    if ($releasePipe.status -ne 'success') {
        throw "Release pipeline failed: $($releasePipe.web_url)"
    }

    ################################################################################
    # Attach packages

    $releaseLinks = Get-ProjectReleaseLink -ProjectId $project.Id `
                                           -TagName $releaseName `
                                           -GitLabApiToken $GitLabApiToken

    if ($releaseLinks.Count -gt 0) {
        Write-Information "This release already has the following packages attached:`n  - $($releaseLinks.name -join "`n  - ")"
    } else {

        Write-Information "Attaching packages to the release"

        try {
            $jobsRsp = Get-PipelineJob -ProjectId $project.Id `
                                       -PipelineId $releasePipe.id `
                                       -GitLabApiToken $GitLabApiToken
        } catch {
            Write-Error "Could not retrieve jobs for the release pipeline ($($releasePipe.id))"
            return
        }

        $jobsRsp | Where-Object { $_.name -match $rm.ReleasePackageRegex } | ForEach-Object {
            if ($_.artifacts_file) {
                Add-ProjectReleaseLink -ProjectId $project.Id `
                                       -TagName $releaseName `
                                       -Name $_.artifacts_file.filename `
                                       -Url ($_.web_url + "/artifacts/download?file_type=archive") `
                                       -GitLabApiToken $GitLabApiToken | Out-Null
                Write-Information "  $($_.artifacts_file.filename)"
            } else {
                Write-Warning "Job '$($_.name)' matched ReleasePackageRegex but had no file artifacts."
            }
        }
        
    }

    Write-Information "Finished $($project.Name)"

}

################################################################################
