[CmdletBinding()]
Param (
    # Project ID
    [Parameter(Mandatory=$true,
               Position=1,
               ValueFromPipeline=$true)]
    [int]$ProjectId,

    # GitLab personal access token (PAT)
    [Parameter(Mandatory=$true,
               Position=2)]
    [ValidateNotNullOrEmpty()]
    [string]$GitLabApiToken,

    # Path to the release manifest. If not set, most recent json
    # file in the './releases' directory is used.
    [Parameter(Mandatory=$false,
               Position=3)]
    [string]$ReleaseManifest,

    # Path to the dotnet-outdated tool for dependency updates
    [Parameter(Mandatory=$false)]
    [string]$DotnetOutdated
)

Begin {

    if (!(Get-Module GitLabApi)) {
        Import-Module (Join-Path $PSScriptRoot GitLabApi) -WarningAction SilentlyContinue
    }

    if (!$ReleaseManifest) {
        $ReleaseManifest = Get-ChildItem -Path ./releases |
            Sort-Object -Property Name -Descending |
            Select-Object -First 1 -ExpandProperty FullName
        Write-Information "ReleaseManifest not specified. Using: $ReleaseManifest"
    }
    
    $rm = Get-Content $ReleaseManifest -Encoding utf8NoBOM | ConvertFrom-Json -Depth 100

}

Process {

    $project = $rm.Projects | Where-Object { $_.Id -eq $ProjectId }

    if (!$project) {
        throw "Could not find project id $ProjectId in the release manifest $ReleaseManifest"
    }

    ################################################################################

    Write-Information "Processing $($project.Name) ($($project.Id))"
    Write-Information $project.WebUrl

    $releaseName = 'v' + $project.ReleaseVersion

    try {
        $existingRelease = Get-ProjectRelease -ProjectId $project.Id `
                                              -TagName $releaseName `
                                              -GitLabApiToken $GitLabApiToken `
                                              -ErrorAction Stop
    } catch {}

    if ($existingRelease) {
        Write-Information "Release $releaseName already exists"
        return
    }

    $alreadyExists = $null
    $alreadyExists = Get-IssueMR -ProjectId $project.Id `
                                 -IssueTitle $project.IssueTitle `
                                 -GitLabApiToken $GitLabApiToken

    if ($alreadyExists) {
        Write-Information ("The release issue already exists:`n" +
                           "Issue         : $($alreadyExists.Issue.web_url)`n" +
                           "Issue state   : $($alreadyExists.Issue.state)`n" +
                           "Merge Request : $($alreadyExists.MergeRequest.web_url)`n" +
                           "MR state      : $($alreadyExists.MergeRequest.state)")
        if ($alreadyExists.Issue.state -ne 'opened') {
            return
        }
        $issue = $alreadyExists
    } else {

        $issueArgs = @{
            ProjectId          = $project.Id
            IssueTitle         = $project.IssueTitle
            IssueLabels        = $rm.IssueMR.IssueLabels
            IssueDescription   = $rm.IssueMR.IssueDescription
            MergeRequestLabels = $rm.IssueMR.MergeRequestLabels
            TargetBranch       = $project.ReleaseBranch
            AssigneeId         = $rm.User.Id
            GitLabApiToken     = $GitLabApiToken
        }

        if ($rm.Epic) {
            $issueArgs['EpicId'] = $rm.Epic.id
        }

        if ($rm.Milestone) {
            $issueArgs['MilestoneId'] = $rm.Milestone.id
        }

        if ($rm.Iteration) {
            $issueArgs['IterationId'] = $rm.Iteration.id
        }

        $issue = $null
        $issue = Add-IssueMR @issueArgs

        if ($null -eq $issue) {
            throw 'Could not create release issue and merge request'
        }

    }

    if (!$project.SkipDependencyUpdate -and $project.DependsOn.Length -gt 0) {
        ./dependencies/Update-Dependencies.ps1 -ProjectId $project.Id `
                                               -IssueTitle $project.IssueTitle `
                                               -IncludeFilter $rm.DependencyFilter `
                                               -GitLabApiToken $GitLabApiToken `
                                               -DotnetOutdated $DotnetOutdated `
                                               -DoNotUpdateDescription `
                                               -NoAutoMerge -Wait
    }

    Write-Information "Checking if the readme has been updated since last release"

    try {
        $readmeUpdates = Get-Commit -ProjectId $project.Id `
                                    -Ref $project.ReleaseBranch `
                                    -Since $project.LastReleaseDate `
                                    -Path 'README.md' `
                                    -GitLabApiToken $GitLabApiToken `
                                    -ErrorAction Stop
    } catch { }

    if (!$readmeUpdates) {
        $readmeWarning = ":exclamation: README.md has not been updated since last release. Please check if it needs updating.`n`n"
    }

    $updateDescription = "Closes #$($issue.Issue.iid)`n`n" + $readmeWarning + $rm.IssueMR.MergeRequestDescription

    Write-Information "Updating the merge request description"

    try {
        Set-MergeRequest -ProjectId $project.Id `
                         -MergeRequestId $issue.MergeRequest.iid `
                         -Description $updateDescription `
                         -GitLabApiToken $GitLabApiToken `
                         -ErrorAction Stop | Out-Null
    } catch {
        Write-Warning "Could not update merge request description. Error: $($_.Exception.Message)"
    }

    Write-Information "Setting approval rule for the merge request"

    try {
        Set-MergeRequestApprovals -ProjectId $project.Id `
                                  -MergeRequestId $issue.MergeRequest.iid `
                                  -ApprovalsRequired 1 `
                                  -GitLabApiToken $GitLabApiToken `
                                  -ErrorAction Stop | Out-Null
    } catch {
        Write-Warning "Could not set approval rule for '$($issue.MergeRequest.title)' merge request"
    }

    ################################################################################

    Write-Information "Updating the changelog"

    try {
        $changelogRsp = Get-ProjectFile -ProjectId $project.id `
                                        -Path 'CHANGELOG.md' `
                                        -Ref $project.ReleaseBranch `
                                        -GitLabApiToken $GitLabApiToken `
                                        -ErrorAction Stop
    } catch {
        Write-Information "No changelog found. Creating new one."
    }

    $changelogPath = Join-Path $PSScriptRoot "$($project.Id)-changelog.md"

    if ($changelogRsp) {
        $changelogAction = 'update'
        if ($changelogRsp.encoding -eq 'base64') {
            $changelogContent = [System.Text.Encoding]::UTF8.GetString(
                [System.Convert]::FromBase64String($changelogRsp.content)
            )
        } else {
            $changelogContent = $changelogRsp.content
        }
        $changelogContent | Out-File -FilePath $changelogPath -Encoding utf8NoBOM
    } else {
        $changelogAction = 'create'
    }

    $changelogArgs = @{
        ProjectId      = $project.Id
        ReleaseVersion = $releaseName
        ReleaseDate    = $rm.Date
        ChangelogPath  = $changelogPath
        TargetBranch   = $project.ReleaseBranch
        GitLabApiToken = $GitLabApiToken
        MaintenanceDescription = $rm.NoChangelogDescription
    }

    if ($project.LastReleaseDate) {
        $changelogArgs['FromDate'] = $project.LastReleaseDate
    }

    if ($project.ChangelogSummary) {
        $changelogArgs['ChangelogSummary'] = $project.ChangelogSummary
    }

    ./New-Changelog.ps1 @changelogArgs

    $newChangelog = (Get-Content -Path $changelogPath -Raw -Encoding utf8NoBOM) -replace "`r`n", "`n"

    $changelogAction = @([pscustomobject]@{
        action    = $changelogAction
        file_path = 'CHANGELOG.md'
        content   = $newChangelog
    })

    Write-Information "Pushing the changelog update"

    $commitRsp = Add-Commit -ProjectId $project.Id `
                            -Branch $issue.Branch.name `
                            -CommitMessage $project.IssueTitle `
                            -Actions $changelogAction `
                            -EncodeContent `
                            -GitLabApiToken $GitLabApiToken

    if ($null -eq $commitRsp) {
        throw "Could not push changelog update"
    }

    Write-Information "Finished $($project.Name)"

}

################################################################################
