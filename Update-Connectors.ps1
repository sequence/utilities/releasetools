[CmdletBinding()]
Param (
    # Path to the Sequence Console project
    [Parameter(Mandatory=$true,
               Position=1)]
    [string]$Path,

    # Upgrade to any prerelease packages that it finds
    [switch]$IncludePrerelease
)

Push-Location $Path
dotnet publish --output ./publish *> $null
$connectors = ./publish/sequence connector list |
    Where-Object { $_ -match '^(Sequence[\w\.]+)' } |
    ForEach-Object { $Matches[1] }
$connectors | ForEach-Object {
    if ($IncludePrerelease) {
        ./publish/sequence connector update --prerelease $_
    } else {
        ./publish/sequence connector update $_
    }
}
Copy-Item -Path ./publish/connectors.json -Destination ./
Pop-Location
